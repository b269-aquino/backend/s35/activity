const express = require("express");

// Mongoose is a package that allows creation of schemas to our model data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://kchristianaquino:admin123@zuitt-bootcamp.jthn0f5.mongodb.net/s35?retryWrites=true&w=majority",
	{
		// allows us to avoid any current and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Allows to handle error when the initial connection is established
let db = mongoose.connection;

// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connnection error"));
// If the connection is successful, out in the console
db.once("open",() => console.log("We're connected to the cloud database"));

// Connecting to MongoDB Atlas END

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// [SECTION] Mongoose Schemas

const taskSchema = new mongoose.Schema({
	name: String,
	user: String,
	email: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}
}) ;

// [SECTION] Mongoose Models
// Modles must be in singular form and capitalized
const Task = mongoose.model("Task", taskSchema);


// Creation of Task Application
// Create a new task 

app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}).then((result,err) => {

		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		} else {
			let newTask = new Task ({
				name: req.body.name
			});
			newTask.save().then((savedTask, saveErr) => {
				if(saveErr){
					return console.Error(saveErr);
				} else {
					return res.status(201).send("New task created!");
				}
			})
		}

	})
});

// Getting all the tasks
/*
BUSINESS LOGIC
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/


app.get("/tasks", (req,res) => {
	Task.find({}).then((result, err) => {
		if(err) { 
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})

});



// ACTIVITY [SECTION]

const userSchema = new mongoose.Schema({
	username: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}
}) ;

const User = mongoose.model("User", userSchema);

app.post("/signup", (req,res) => {
	User.findOne({username: req.body.username, password: req.body.password}).then((result, err) => {
		if(result != null && result.username == req.body.username) {
			return res.send("Cannot create User ")
		} else {
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save().then((savedUser, saveErr) => {
				if (saveErr) {
					return console.log(saveErr)
				} else {
					return res.status(201).send("New user registered!")
				}
			})

		}
	})
});


app.get("/signup", (req,res) => {
	User.find({}).then((result, err) => {
		if(err) { 
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})

});


app.listen(port, () => console.log(`Server running at port ${port}`))


// REFERENCE
// https://mongoosejs.com/docs/models.html